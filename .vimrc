set pastetoggle=<F1>
set shiftwidth=4
set tabstop=4
set expandtab
set updatecount=0
set wrapscan
set hlsearch
set incsearch
set ignorecase
set smartcase
set ruler
set report=1
set noicon
set showmatch
set nostartofline
set clipboard=autoselect
set visualbell t_vb=
set viminfo=
set history=500
set laststatus=0
set autoindent
nnoremap <CR> :noh<CR>
set t_ut=


syntax enable
autocmd ColorScheme angr highlight Normal ctermbg=235
colorscheme angr
